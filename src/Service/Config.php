<?php

namespace Kudze\NrdbvsNeo4jDemo\Service;

use Symfony\Component\Dotenv\Dotenv;

/**
 * Configuration manager.
 */
class Config
{
    private Dotenv $dotEnv;

    public function __construct(Dotenv $dotEnv)
    {
        $this->dotEnv = $dotEnv;

        $this->loadConfigs();
    }

    protected function loadConfigs() {
        $this->dotEnv->load(__DIR__ . '/../../.env');
    }

    /**
     * @param string $var
     * @return mixed
     */
    public function get(string $var): mixed
    {
        return $_ENV[$var];
    }

    /**
     * @return Dotenv
     */
    public function getDotEnv(): Dotenv
    {
        return $this->dotEnv;
    }
}