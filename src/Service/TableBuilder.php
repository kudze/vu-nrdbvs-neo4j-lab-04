<?php

namespace Kudze\NrdbvsNeo4jDemo\Service;

use Kudze\NrdbvsNeo4jDemo\Model\City;
use Kudze\NrdbvsNeo4jDemo\Model\Collections\Path;
use Kudze\NrdbvsNeo4jDemo\Model\Person;
use Kudze\NrdbvsNeo4jDemo\Model\Relationship\Road;
use LucidFrame\Console\ConsoleTable;

class TableBuilder
{

    public function buildTableOfPeople(array $people): string
    {
        $table = new ConsoleTable();
        $table
            ->addHeader('ID')
            ->addHeader('First Name')
            ->addHeader('Last Name');
        foreach ($people as $person) {
            /** @var Person $person */
            $table->addRow()
                ->addColumn($person->getId())
                ->addColumn($person->getFirstName())
                ->addColumn($person->getLastName());
        }

        $table->setPadding(2);
        return $table->getTable();
    }

    public function buildTableOfCities(array $cities): string
    {
        $table = new ConsoleTable();
        $table
            ->addHeader('ID')
            ->addHeader('Name')
            ->addHeader('Population');

        foreach($cities as $city) {
            /** @var City $city */
            $table->addRow()
                ->addColumn($city->getId())
                ->addColumn($city->getName())
                ->addColumn($city->getPopulation());
        }

        $table->setPadding(2);
        return $table->getTable();
    }

    public function buildTableOfPath(Path $path): string
    {
        $table = new ConsoleTable();
        $table
            ->addHeader('Road Id')
            ->addHeader('Source City Name')
            ->addHeader('Destination City Name')
            ->addHeader('Roads length');

        foreach($path->getRoads() as $road) {
            /** @var Road $road */
            $table->addRow()
                ->addColumn($road->getId())
                ->addColumn($road->getRoadFrom()->getName())
                ->addColumn($road->getRoadTo()->getName())
                ->addColumn($road->getLength());
        }

        $table->setPadding(2);
        return $table->getTable();
    }

}