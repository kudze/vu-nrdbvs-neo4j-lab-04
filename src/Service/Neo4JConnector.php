<?php

namespace Kudze\NrdbvsNeo4jDemo\Service;

use Bolt\Bolt;
use Bolt\connection\IConnection;
use Bolt\connection\StreamSocket;

/**
 * Class thats responsible for connecting us to neo4j
 */
class Neo4JConnector
{
    private Config $config;
    private IConnection $connection;
    private Bolt $bolt;

    public function __construct(Config $config)
    {
        $this->config = $config;

        $this->connection = new StreamSocket($this->getIp(), $this->getPort(), $this->getTimeout());

        $this->bolt = new Bolt($this->connection);
        $this->bolt->setProtocolVersions($this->getProtocolVersion());
        $this->bolt->init($this->getDatabaseName(), $this->getUsername(), $this->getPassword());
    }

    protected function getIp(): string
    {
        return $this->config->get('NEO4J_IP');
    }

    protected function getPort(): int
    {
        return $this->config->get('NEO4J_PORT');
    }

    protected function getTimeout(): int
    {
        return $this->config->get('NEO4J_TIMEOUT');
    }

    protected function getProtocolVersion(): string
    {
        return $this->config->get('NEO4J_VERSION');
    }

    protected function getDatabaseName(): string
    {
        return $this->config->get('NEO4J_VERSION');
    }

    protected function getUsername(): string
    {
        return $this->config->get('NEO4J_USERNAME');
    }

    protected function getPassword(): string
    {
        return $this->config->get('NEO4J_PASSWORD');
    }

    /**
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * @return IConnection|StreamSocket
     */
    public function getConnection(): IConnection|StreamSocket
    {
        return $this->connection;
    }

    /**
     * @return Bolt
     */
    public function getBolt(): Bolt
    {
        return $this->bolt;
    }
}