<?php

namespace Kudze\NrdbvsNeo4jDemo\Service;

use Faker\Factory;
use Faker\Generator;
use Kudze\NrdbvsNeo4jDemo\Model\City;
use Kudze\NrdbvsNeo4jDemo\Model\Person;

class Faker
{
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create('lt_LT');
    }

    public function fakeCity(): City
    {
        $city = new City();

        $city->setName($this->faker->city())
             ->setPopulation($this->fakePopulation());

        return $city;
    }

    /**
     * This guarantees that in returned array the cities will have unique name.
     *
     * @param int $n
     * @return array
     */
    public function fakeNUniqueCities(int $n): array
    {
        $uniqueGenerator = $this->createUniqueGenerator();

        $result = [];

        for($i = 0; $i < $n; $i++)
        {
            $city = new City();
            $city
                ->setName($uniqueGenerator->city())
                ->setPopulation($this->fakePopulation());

            $result[] = $city;
        }

        return $result;
    }

    /**
     * This guarantees that in returned array the people will have unique names.
     *
     * @param int $n
     * @return array
     */
    public function fakeNUniquePeople(int $n): array
    {
        $uniqueGenerator = $this->createUniqueGenerator();

        $result = [];
        for($i = 0; $i < $n; $i++)
        {
            $gender = $this->fakeGender();

            $person = new Person();
            $person->setFirstName($uniqueGenerator->firstName($gender))
                ->setLastName($uniqueGenerator->lastName($gender));

            $result[] = $person;
        }

        return $result;
    }

    public function getRandomElementFromArray(array $array): mixed
    {
        $key = array_rand($array, 1);
        return $array[$key];
    }

    protected function fakePopulation(int $from = 1000, int $to = 500000): int
    {
        return $this->faker->numberBetween($from, $to);
    }

    protected function fakeGender(): string
    {
        return $this->faker->randomElement(['male', 'female']);
    }

    protected function createUniqueGenerator(): \Faker\UniqueGenerator
    {
        return $this->faker->unique();
    }

    /**
     * @return Generator
     */
    public function getFaker(): Generator
    {
        return $this->faker;
    }
}