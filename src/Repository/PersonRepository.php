<?php

namespace Kudze\NrdbvsNeo4jDemo\Repository;

use Bolt\structures\Node;
use Kudze\NrdbvsNeo4jDemo\Model\Person;
use Kudze\NrdbvsNeo4jDemo\Model\Relationship\LivesIn;
use Kudze\NrdbvsNeo4jDemo\Repository\Abstract\Neo4JRepository;
use RuntimeException;

class PersonRepository extends Neo4JRepository
{
    /**
     * This will insert relationship between city and user, so user lives in city.
     *
     * @param LivesIn $livesIn
     */
    public function insertLivesIn(LivesIn $livesIn)
    {
        $bolt = $this->getBolt();
        $bolt->run(
            'MATCH (a:Person), (b:City) WHERE ID(a) = $personId AND ID(b) = $cityId CREATE (a)-[r:livesIn]->(b) RETURN r',
            [
                'personId' => $livesIn->getPerson()->getId(),
                'cityId' => $livesIn->getCity()->getId()
            ]
        );

        $res = $bolt->pull();
        $livesIn->setId($res[0][0]->id());
    }

    /**
     * This will not check if person already exists at the database.
     *
     * @param Person $person
     * @throws \Exception
     */
    public function insertPerson(Person $person)
    {
        $bolt = $this->getBolt();
        $bolt->run(
            'CREATE (n:Person {firstName: $firstName, lastName: $lastName}) RETURN ID(n)',
            [
                'firstName' => $person->getFirstName(),
                'lastName' => $person->getLastName()
            ]
        );

        $res = $bolt->pull();
        $person->setId($res[0][0]);
    }

    public function findPersonByID(int $id): Person
    {
        $bolt = $this->getBolt();
        $bolt->run(
            'MATCH(a:Person) WHERE id(a) = $personId RETURN a',
            [
                'personId' => $id
            ]
        );

        return $this->buildModelFromPull($bolt->pull());
    }

    public function listAllPeople(): array
    {
        $bolt = $this->getBolt();
        $bolt->run(
            'MATCH (n:Person) RETURN n'
        );

        return $this->buildModelArrayFromPull($bolt->pull());
    }

    /**
     * Will use contains for matching.
     *
     * @param string $firstName
     * @return array
     * @throws \Exception
     */
    public function findPeopleByFirstName(string $firstName): array
    {
        $bolt = $this->getBolt();
        $bolt->run(
            'MATCH (n:Person) WHERE n.firstName CONTAINS $firstName RETURN n',
            [
                'firstName' => $firstName
            ]
        );

        return $this->buildModelArrayFromPull($bolt->pull());
    }

    /**
     * Will find all citizens (people) of a city.
     *
     * @param string $cityName
     * @return array
     * @throws \Exception
     */
    public function listCitizensOfCity(string $cityName): array
    {
        $bolt = $this->getBolt();
        $bolt->run(
            'MATCH (p:Person)-[:livesIn]->(c:City) WHERE c.name CONTAINS $name RETURN p',
            [
                'name' => $cityName
            ]
        );

        return $this->buildModelArrayFromPull($bolt->pull());
    }

    /**
     * Will use contains for matching.
     *
     * @param string $firstName
     * @param string $lastName
     * @return array
     * @throws \Exception
     */
    public function findPeopleByFirstAndLastNames(string $firstName, string $lastName): array
    {
        $bolt = $this->getBolt();
        $bolt->run(
            'MATCH (n:Person) WHERE n.firstName CONTAINS $firstName AND n.lastName CONTAINS $lastName RETURN n',
            [
                'firstName' => $firstName,
                'lastName' => $lastName
            ]
        );

        return $this->buildModelArrayFromPull($bolt->pull());
    }

    protected function buildModelFromRow(array $row): Person
    {
        $node = $row[0];
        if (!($node instanceof Node))
            throw new RuntimeException("Unexpected entry");

        $properties = $node->properties();

        $person = new Person();
        $person->setId($node->id())
            ->setFirstName($properties['firstName'])
            ->setLastName($properties['lastName']);
        return $person;
    }
}