<?php

namespace Kudze\NrdbvsNeo4jDemo\Repository;

use Bolt\structures\Node;
use Bolt\structures\Relationship;
use Kudze\NrdbvsNeo4jDemo\Exception\ModelNotFoundException;
use Kudze\NrdbvsNeo4jDemo\Model\City;
use Kudze\NrdbvsNeo4jDemo\Model\Collections\Path;
use Kudze\NrdbvsNeo4jDemo\Model\Relationship\Road;
use Kudze\NrdbvsNeo4jDemo\Repository\Abstract\Neo4JRepository;
use Kudze\NrdbvsNeo4jDemo\Service\Neo4JConnector;

class PathRepository extends Neo4JRepository
{
    private CityRepository $cityRepository;

    public function __construct(Neo4JConnector $connector, CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;

        parent::__construct($connector);
    }

    public function listAllPathsFromCityToCity(int $cityFromId, int $cityToId): array
    {
        $bolt = $this->getBolt();

        $bolt->run(
            <<<'EOD'
            MATCH (source:City) WHERE ID(source) = $sourceId
            MATCH (dest:City) WHERE ID(dest) = $destId
            MATCH p = (source)-[:roadTo*1..]->(dest)
            RETURN nodes(p), relationships(p)
            EOD,
            [
                'sourceId' => $cityFromId,
                'destId' => $cityToId
            ]
        );

        return $this->buildModelArrayFromPull($bolt->pullAll());
    }

    public function findShortestPathFromCityToCity(int $cityFromId, int $cityToId): Path
    {
        $bolt = $this->getBolt();

        $bolt->run(
            //<<<'EOD'
            //MATCH (source:City) WHERE ID(source) = $sourceId
            //MATCH (dest:City) WHERE ID(dest) = $destId
            //MATCH p = shortestPath((source)-[:roadTo*1..]->(dest))
            //RETURN nodes(p), relationships(p) LIMIT 1
            //EOD,
            <<<'EOD'
            MATCH (source:City) WHERE ID(source) = $sourceId
            MATCH (dest:City) WHERE ID(dest) = $destId
            MATCH p = (source)-[:roadTo*1..]->(dest)
            WITH *, reduce(totalDist = 0, n in relationships(p) | totalDist + n.length) AS shortest
            RETURN nodes(p), relationships(p) ORDER BY shortest LIMIT 1
            EOD,
            [
                'sourceId' => $cityFromId,
                'destId' => $cityToId
            ]
        );

        return $this->buildModelFromPull($bolt->pull());
    }

    public function aggregateShortestPathDistanceFromCityToCity(int $cityFromId, int $cityToId): float
    {
        $bolt = $this->getBolt();

        $bolt->run(
            <<<'EOD'
            MATCH (source:City) WHERE ID(source) = $sourceId
            MATCH (dest:City) WHERE ID(dest) = $destId
            MATCH p = (source)-[:roadTo*1..]->(dest)
            WITH *, reduce(totalDist = 0, n in relationships(p) | totalDist + n.length) AS shortest
            RETURN shortest ORDER BY shortest
            EOD,
            [
                'sourceId' => $cityFromId,
                'destId' => $cityToId
            ]
        );

        $res = $bolt->pull();
        if($res === 1)
            throw new ModelNotFoundException();

        return $res[0][0];
    }

    protected function buildModelFromRow(array $row): Path
    {
        $citiesData = $row[0];
        $roadsData = $row[1];

        $cities = [];
        foreach($citiesData as $node)
        {
            if(!($node instanceof Node))
                continue;

            /** @var City $city */
            $city = $this->cityRepository->buildModelFromRow([$node]);
            $cities[$city->getId()] = $city;
        }

        $roads = [];
        foreach($roadsData as $relationship) {
            if(!($relationship instanceof Relationship))
                continue;

            $properties = $relationship->properties();

            $road = new Road();
            $road
                ->setId($relationship->id())
                ->setRoadFrom($cities[$relationship->startNodeId()])
                ->setRoadTo($cities[$relationship->endNodeId()])
                ->setLength($properties['length']);
            $roads[] = $road;
        }

        return new Path($roads);
    }
}