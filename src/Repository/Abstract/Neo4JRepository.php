<?php

namespace Kudze\NrdbvsNeo4jDemo\Repository\Abstract;

use Bolt\Bolt;
use Kudze\NrdbvsNeo4jDemo\Exception\InaccuracyException;
use Kudze\NrdbvsNeo4jDemo\Exception\ModelNotFoundException;
use Kudze\NrdbvsNeo4jDemo\Service\Neo4JConnector;

abstract class Neo4JRepository
{
    private Neo4JConnector $connector;

    public function __construct(Neo4JConnector $connector)
    {
        $this->connector = $connector;
    }

    /**
     * @return Neo4JConnector
     */
    public function getConnector(): Neo4JConnector
    {
        return $this->connector;
    }

    public function getBolt(): Bolt
    {
        return $this->getConnector()->getBolt();
    }

    /**
     * Deletes all nodes and relationships.
     */
    public function deleteEverything()
    {
        $bolt = $this->getBolt();

        $bolt->run('MATCH (n) DETACH DELETE n');
        $bolt->pull();
    }

    protected function buildModelArrayFromPull(array $pull): array
    {
        $result = [];

        foreach($pull as $entry)
        {
            if(!is_array($entry) || empty($entry) || array_key_exists('bookmark', $entry))
                continue;

            $result[] = $this->buildModelFromRow($entry);
        }

        return $result;
    }

    protected function buildModelFromPull(array $pull): mixed
    {
        $result = $this->buildModelArrayFromPull($pull);
        $result_count = count($result);

        if($result_count === 0)
            throw new ModelNotFoundException();

        if($result_count > 1)
            throw new InaccuracyException();

        return $result[0];
    }

    protected abstract function buildModelFromRow(array $row): mixed;
}