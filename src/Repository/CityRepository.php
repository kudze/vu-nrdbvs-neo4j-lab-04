<?php

namespace Kudze\NrdbvsNeo4jDemo\Repository;

use Bolt\structures\Node;
use RuntimeException;
use Kudze\NrdbvsNeo4jDemo\Model\City;
use Kudze\NrdbvsNeo4jDemo\Model\Relationship\Road;
use Kudze\NrdbvsNeo4jDemo\Repository\Abstract\Neo4JRepository;

class CityRepository extends Neo4JRepository
{
    /**
     * This will insert a road between two cities.
     *
     * @param Road $road
     */
    public function insertRoad(Road $road)
    {
        $bolt = $this->getBolt();
        $bolt->run(
            <<<'EOD'
                MATCH (a:City) WHERE ID(a) = $cityFromId
                MATCH (b:City) WHERE ID(b) = $cityToId 
                CREATE (a)-[r:roadTo {length: $length}]->(b) 
                RETURN r
            EOD,
            [
                'cityFromId' => $road->getRoadFrom()->getId(),
                'cityToId' => $road->getRoadTo()->getId(),
                'length' => $road->getLength()
            ]
        );

        $res = $bolt->pull();
        $road->setId($res[0][0]->id());
    }

    /**
     * This will not check if city already exists at the database.
     *
     * This will edit city model and add id to it.
     *
     * @param City $city
     * @throws \Exception
     */
    public function insertCity(City $city)
    {
        $bolt = $this->getBolt();
        $bolt->run(
            <<<'EOD'
            CREATE (n:City {name: $name, population: $population}) 
            RETURN ID(n),
            EOD,
            [
                'name' => $city->getName(),
                'population' => $city->getPopulation()
            ]
        );

        $res = $bolt->pull();
        $city->setId($res[0][0]);
    }

    public function listAllCities(): array
    {
        $bolt = $this->getBolt();
        $bolt->run(
            'MATCH(c:City) RETURN c'
        );

        return $this->buildModelArrayFromPull($bolt->pull());
    }

    /**
     * Will try to match single city to a name.
     * If more than one is matched result is randomly picked.
     *
     * @param string $name
     * @return City
     * @throws \Exception
     */
    public function findCityByName(string $name): City
    {
        $bolt = $this->getBolt();
        $bolt->run(
            'Match (c:City) WHERE c.name CONTAINS $name RETURN c',
            [
                'name' => $name
            ]
        );

        return $this->buildModelFromPull($bolt->pull());
    }

    public function findCityUserLivesIn(int $userId): City
    {
        $bolt = $this->getBolt();
        $bolt->run(
            'MATCH (p:Person)-[:livesIn]->(c:City) WHERE id(p) = $userId RETURN c',
            [
                'userId' => $userId
            ]
        );

        return $this->buildModelFromPull($bolt->pull());
    }

    public function buildModelFromRow(array $row): City
    {
        $node = $row[0];
        if (!($node instanceof Node))
            throw new RuntimeException("Unexpected entry!");

        $properties = $node->properties();

        $city = new City();
        $city->setId($node->id())
            ->setPopulation($properties['population'])
            ->setName($properties['name']);

        return $city;
    }
}