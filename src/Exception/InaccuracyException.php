<?php

namespace Kudze\NrdbvsNeo4jDemo\Exception;

use \RuntimeException;

/**
 * This exception gets thrown whenether function is supposed to return only one model, but database returns more than one.
 */
class InaccuracyException extends RuntimeException
{

}