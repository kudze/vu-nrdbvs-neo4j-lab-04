<?php

namespace Kudze\NrdbvsNeo4jDemo\Exception;

use \RuntimeException;

/**
 * This exception gets thrown whenether function is supposed to returns a model but model was not found!
 */
class ModelNotFoundException extends RuntimeException
{

}