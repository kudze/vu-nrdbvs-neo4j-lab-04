<?php

namespace Kudze\NrdbvsNeo4jDemo\Model;

use Kudze\NrdbvsNeo4jDemo\Model\Abstract\AbstractModel;

class Person extends AbstractModel
{
    private string $firstName;
    private string $lastName;

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Person
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Person
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function buildFullName(): string
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }
}