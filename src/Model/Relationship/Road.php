<?php

namespace Kudze\NrdbvsNeo4jDemo\Model\Relationship;

use Kudze\NrdbvsNeo4jDemo\Model\Abstract\AbstractModel;
use Kudze\NrdbvsNeo4jDemo\Model\City;

class Road extends AbstractModel
{
    private City $roadFrom;
    private City $roadTo;

    private float $length;

    /**
     * @return City
     */
    public function getRoadFrom(): City
    {
        return $this->roadFrom;
    }

    /**
     * @param City $roadFrom
     * @return self
     */
    public function setRoadFrom(City $roadFrom): self
    {
        $this->roadFrom = $roadFrom;

        return $this;
    }

    /**
     * @return City
     */
    public function getRoadTo(): City
    {
        return $this->roadTo;
    }

    /**
     * @param City $roadTo
     * @return self
     */
    public function setRoadTo(City $roadTo): self
    {
        $this->roadTo = $roadTo;

        return $this;
    }

    /**
     * length as float in km.
     *
     * @return float
     */
    public function getLength(): float
    {
        return $this->length;
    }

    /**
     * length as float in km.
     *
     * @param float $length
     */
    public function setLength(float $length): self
    {
        $this->length = $length;

        return $this;
    }
}