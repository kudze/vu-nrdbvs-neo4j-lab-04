<?php

namespace Kudze\NrdbvsNeo4jDemo\Model\Relationship;

use Kudze\NrdbvsNeo4jDemo\Model\Abstract\AbstractModel;
use Kudze\NrdbvsNeo4jDemo\Model\City;
use Kudze\NrdbvsNeo4jDemo\Model\Person;

class LivesIn extends AbstractModel
{
    private City $city;
    private Person $person;

    /**
     * @return City
     */
    public function getCity(): City
    {
        return $this->city;
    }

    /**
     * @param City $city
     * @return LivesIn
     */
    public function setCity(City $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Person
     */
    public function getPerson(): Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return LivesIn
     */
    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }
}