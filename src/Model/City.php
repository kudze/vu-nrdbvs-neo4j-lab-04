<?php

namespace Kudze\NrdbvsNeo4jDemo\Model;

use Kudze\NrdbvsNeo4jDemo\Model\Abstract\AbstractModel;

class City extends AbstractModel
{
    private string $name;
    private int $population;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return City
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getPopulation(): int
    {
        return $this->population;
    }

    /**
     * @param int $population
     * @return City
     */
    public function setPopulation(int $population): self
    {
        $this->population = $population;

        return $this;
    }
}