<?php

namespace Kudze\NrdbvsNeo4jDemo\Model\Abstract;

trait IdTrait
{
    private int $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }
}