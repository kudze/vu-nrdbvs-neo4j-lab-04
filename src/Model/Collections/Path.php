<?php

namespace Kudze\NrdbvsNeo4jDemo\Model\Collections;

/**
 * Collection of roads.
 */
class Path
{
    private array $roads;

    public function __construct(array $roads)
    {
        $this->roads = $roads;
    }

    /**
     * @return array
     */
    public function getRoads(): array
    {
        return $this->roads;
    }
}