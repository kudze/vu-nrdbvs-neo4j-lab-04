<?php

namespace Kudze\NrdbvsNeo4jDemo\Command;

use Kudze\NrdbvsNeo4jDemo\Repository\CityRepository;
use Kudze\NrdbvsNeo4jDemo\Repository\PersonRepository;
use Kudze\NrdbvsNeo4jDemo\Service\Neo4JConnector;
use Kudze\NrdbvsNeo4jDemo\Service\TableBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Neo4JListAllCitiesCommand extends Command
{
    private CityRepository $cityRepository;
    private TableBuilder $tableBuilder;

    public function __construct(CityRepository $cityRepository, TableBuilder $tableBuilder)
    {
        $this->cityRepository = $cityRepository;
        $this->tableBuilder = $tableBuilder;

        parent::__construct();
    }

    public function configure()
    {
        $this->setName("neo4j:list:cities");
        $this->setDescription("Lists all cities in neo4j database!");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $cities = $this->cityRepository->listAllCities();
        $table = $this->tableBuilder->buildTableOfCities($cities);

        $output->writeln("All cities in database:");
        $output->writeln($table);

        return self::SUCCESS;
    }
}