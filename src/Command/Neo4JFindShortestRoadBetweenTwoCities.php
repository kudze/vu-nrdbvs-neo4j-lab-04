<?php

namespace Kudze\NrdbvsNeo4jDemo\Command;

use Kudze\NrdbvsNeo4jDemo\Exception\InaccuracyException;
use Kudze\NrdbvsNeo4jDemo\Exception\ModelNotFoundException;
use Kudze\NrdbvsNeo4jDemo\Repository\CityRepository;
use Kudze\NrdbvsNeo4jDemo\Repository\PathRepository;
use Kudze\NrdbvsNeo4jDemo\Service\TableBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Neo4JFindShortestRoadBetweenTwoCities extends Command
{
    private CityRepository $cityRepository;
    private PathRepository $pathRepository;
    private TableBuilder $tableBuilder;

    public function __construct(CityRepository $cityRepository, PathRepository $pathRepository, TableBuilder $tableBuilder)
    {
        $this->cityRepository = $cityRepository;
        $this->pathRepository = $pathRepository;
        $this->tableBuilder = $tableBuilder;

        parent::__construct();
    }

    public function configure()
    {
        $this->setName('neo4j:find:road:shortest');
        $this->setDescription('Searches for shortest road between two cities!');

        $this->addArgument('city_source_name', InputArgument::REQUIRED, "City from name");
        $this->addArgument('city_destination_name', InputArgument::REQUIRED, "City to name");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $cityFromName = $input->getArgument('city_source_name');
        $cityToName = $input->getArgument('city_destination_name');

        try {
            $cityFrom = $this->cityRepository->findCityByName($cityFromName);
        } catch (ModelNotFoundException) {
            $output->writeln("Source city not found!");
            return self::FAILURE;
        } catch (InaccuracyException) {
            $output->writeln("Source city name too ambiguous!");
            return self::FAILURE;
        }

        try {
            $cityTo = $this->cityRepository->findCityByName($cityToName);
        } catch (ModelNotFoundException) {
            $output->writeln("Destination city not found!");
            return self::FAILURE;
        } catch (InaccuracyException) {
            $output->writeln("Destination city name too ambiguous!");
            return self::FAILURE;
        }

        try {
            $path = $this->pathRepository->findShortestPathFromCityToCity($cityFrom->getId(), $cityTo->getId());
        } catch (ModelNotFoundException) {
            $output->writeln("Path does not exist!");
            return self::FAILURE;
        }

        $cityFromFullName = $cityFrom->getName();
        $cityToFullName = $cityTo->getName();

        $output->writeln("This is the shortestPath from $cityFromFullName to $cityToFullName");
        $table = $this->tableBuilder->buildTableOfPath($path);
        $output->writeln($table);

        return self::SUCCESS;
    }

}