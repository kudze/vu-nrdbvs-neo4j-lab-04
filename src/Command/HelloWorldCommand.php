<?php

namespace Kudze\NrdbvsNeo4jDemo\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Simple hello world command for testing.
 */
class HelloWorldCommand extends Command
{

    public function configure()
    {
        $this->setName("hello");
        $this->setDescription("Prints hello world!");
    }

    public function execute(InputInterface $input, OutputInterface $output) : int
    {
        $output->writeln("Hello world!");

        return self::SUCCESS;
    }

}