<?php

namespace Kudze\NrdbvsNeo4jDemo\Command;

use Kudze\NrdbvsNeo4jDemo\Model\City;
use Kudze\NrdbvsNeo4jDemo\Model\Relationship\LivesIn;
use Kudze\NrdbvsNeo4jDemo\Model\Relationship\Road;
use Kudze\NrdbvsNeo4jDemo\Repository\CityRepository;
use Kudze\NrdbvsNeo4jDemo\Repository\PersonRepository;
use Kudze\NrdbvsNeo4jDemo\Service\Faker;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Neo4JSeedCommand extends Command
{
    private Faker $faker;
    private CityRepository $cityRepository;
    private PersonRepository $personRepository;

    public function __construct(Faker $faker, CityRepository $cityRepository, PersonRepository $personRepository)
    {
        $this->faker = $faker;
        $this->cityRepository = $cityRepository;
        $this->personRepository = $personRepository;

        parent::__construct();
    }

    public function configure()
    {
        $this->setName("neo4j:seed");
        $this->setDescription("Seeds database with demo data!");

        $this->addArgument('cities_count', InputArgument::OPTIONAL, "How many cities to add?", 5);
        $this->addArgument("roads_count", InputArgument::OPTIONAL, "How many roads to add?", 8);
        $this->addArgument('people_count', InputArgument::OPTIONAL, "How many peoples to add?", 20);
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $cities_count = (int)$input->getArgument('cities_count');
        $people_count = (int)$input->getArgument('people_count');
        $roads_count = (int)$input->getArgument('roads_count');

        $output->writeln("Generating $cities_count cities...");
        $cities = $this->faker->fakeNUniqueCities($cities_count);
        foreach ($cities as $city)
            $this->cityRepository->insertCity($city);

        $output->writeln("Generating $roads_count roads...");
        $generatedHistory = [];
        for ($i = 0; $i < $roads_count; $i++) {
            /** @var City $randomCity1 */
            $randomCity1 = $this->faker->getRandomElementFromArray($cities);

            /** @var City $randomCity2 */
            $randomCity2 = $this->faker->getRandomElementFromArray($cities);

            $currGeneration = [$randomCity1->getId(), $randomCity2->getId()];

            //Guarantees no roads with same source and destination, and no repeated roads. (Inverted roads are allowed through)
            if ($randomCity1->getId() === $randomCity2->getId() || in_array($currGeneration, $generatedHistory)) {
                $i--;
                continue;
            }

            $generatedHistory[] = $currGeneration;

            $road = new Road();
            $road
                ->setRoadFrom($randomCity1)
                ->setRoadTo($randomCity2)
                ->setLength($this->faker->getFaker()->randomFloat(2, 10, 200));
            $this->cityRepository->insertRoad($road);
        }

        $output->writeln("Generating $people_count persons...");
        $people = $this->faker->fakeNUniquePeople($people_count);
        foreach ($people as $person) {
            $this->personRepository->insertPerson($person);

            $randomCity = $this->faker->getRandomElementFromArray($cities);

            $livesIn = new LivesIn();
            $livesIn->setCity($randomCity)->setPerson($person);

            $this->personRepository->insertLivesIn($livesIn);
        }

        return self::SUCCESS;
    }
}