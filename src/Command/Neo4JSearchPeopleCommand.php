<?php

namespace Kudze\NrdbvsNeo4jDemo\Command;

use Kudze\NrdbvsNeo4jDemo\Repository\PersonRepository;
use Kudze\NrdbvsNeo4jDemo\Service\TableBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Neo4JSearchPeopleCommand extends Command
{
    private PersonRepository $personRepository;
    private TableBuilder $tableBuilder;

    public function __construct(PersonRepository $personRepository, TableBuilder $tableBuilder)
    {
        $this->personRepository = $personRepository;
        $this->tableBuilder = $tableBuilder;

        parent::__construct();
    }

    public function configure()
    {
        $this->setName("neo4j:search:people:name");
        $this->setDescription("Searches for peoples by their first and/or last name in database!");

        $this->addArgument('first_name', InputArgument::REQUIRED, "First name that you want to search by");
        $this->addArgument('last_name', InputArgument::OPTIONAL, "Last name that you want to search by");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $firstName = $input->getArgument('first_name');
        $lastName = $input->getArgument('last_name');

        $people = [];
        if($lastName === null)
            $people = $this->personRepository->findPeopleByFirstName($firstName);
        else
            $people = $this->personRepository->findPeopleByFirstAndLastNames($firstName, $lastName);

        $table = $this->tableBuilder->buildTableOfPeople($people);

        $peopleCount = count($people);
        $output->writeln("Found $peopleCount by criteria:");
        $output->writeln($table);

        return self::SUCCESS;
    }
}