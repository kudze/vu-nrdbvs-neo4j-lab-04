<?php

namespace Kudze\NrdbvsNeo4jDemo\Command;

use Kudze\NrdbvsNeo4jDemo\Exception\ModelNotFoundException;
use Kudze\NrdbvsNeo4jDemo\Repository\CityRepository;
use Kudze\NrdbvsNeo4jDemo\Repository\PersonRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Neo4JSearchPersonCityCommand extends Command
{
    private PersonRepository $personRepository;
    private CityRepository $cityRepository;

    public function __construct(PersonRepository $personRepository, CityRepository $cityRepository)
    {
        $this->personRepository = $personRepository;
        $this->cityRepository = $cityRepository;

        parent::__construct();
    }

    public function configure()
    {
        $this->setName("neo4j:search:person:city");
        $this->setDescription("Searches person by id and shows city where he lives in!");

        $this->addArgument('person_id', InputArgument::REQUIRED, 'Id of a person');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $personId = $input->getArgument('person_id');

        try {
            $person = $this->personRepository->findPersonByID($personId);
        } catch (ModelNotFoundException) {
            $output->writeln("Given user id was not found!");
            return self::FAILURE;
        }

        try {
            $city = $this->cityRepository->findCityUserLivesIn($personId);
        } catch (ModelNotFoundException) {
            $output->writeln("We dont know where given user id lives!");
            return self::FAILURE;
        }

        $personFullName = $person->buildFullName();
        $cityName = $city->getName();
        $output->writeln("$personFullName lives in $cityName");
        return self::SUCCESS;
    }
}