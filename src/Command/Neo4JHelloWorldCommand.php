<?php

namespace Kudze\NrdbvsNeo4jDemo\Command;

use Kudze\NrdbvsNeo4jDemo\Service\Neo4JConnector;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Neo4JHelloWorldCommand extends Command
{
    private Neo4JConnector $connector;
    public function __construct(Neo4JConnector $connector)
    {
        $this->connector = $connector;

        parent::__construct();
    }

    public function configure()
    {
        $this->setName("neo4j:ping");
        $this->setDescription("Pings neo4j server and checks if we are able to connect!");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("If you're seeing this it means we are connected successfully!");

        return self::SUCCESS;
    }
}