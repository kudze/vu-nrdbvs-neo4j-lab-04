<?php

namespace Kudze\NrdbvsNeo4jDemo\Command;

use Kudze\NrdbvsNeo4jDemo\Exception\InaccuracyException;
use Kudze\NrdbvsNeo4jDemo\Exception\ModelNotFoundException;
use Kudze\NrdbvsNeo4jDemo\Repository\CityRepository;
use Kudze\NrdbvsNeo4jDemo\Repository\PersonRepository;
use Kudze\NrdbvsNeo4jDemo\Service\TableBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Neo4JSearchCityCitizensCommand extends Command
{
    private PersonRepository $personRepository;
    private CityRepository $cityRepository;
    private TableBuilder $tableBuilder;

    public function __construct(PersonRepository $personRepository, CityRepository $cityRepository, TableBuilder $tableBuilder)
    {
        $this->personRepository = $personRepository;
        $this->cityRepository = $cityRepository;
        $this->tableBuilder = $tableBuilder;

        parent::__construct();
    }

    public function configure()
    {
        $this->setName("neo4j:search:city:citizens");
        $this->setDescription("Searches for persons living in a city!");

        $this->addArgument('city_name', InputArgument::REQUIRED, 'Title of a city');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $cityName = $input->getArgument('city_name');
        try {
            $city = $this->cityRepository->findCityByName($cityName);
        } catch (ModelNotFoundException) {
            $output->writeln("Didn't find a city by given name!");
            return self::FAILURE;
        } catch(InaccuracyException) {
            $output->writeln("City name is too ambiguous!");
            return self::FAILURE;
        }

        $people = $this->personRepository->listCitizensOfCity($cityName);
        $people_count = count($people);
        $table = $this->tableBuilder->buildTableOfPeople($people);

        $fullCityName = $city->getName();
        $output->writeln("$fullCityName has $people_count citizens:");
        $output->writeln($table);

        return self::SUCCESS;
    }
}