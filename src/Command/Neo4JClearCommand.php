<?php

namespace Kudze\NrdbvsNeo4jDemo\Command;

use Kudze\NrdbvsNeo4jDemo\Repository\CityRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Neo4JClearCommand extends Command
{
    private CityRepository $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;

        parent::__construct();
    }

    public function configure()
    {
        $this->setName("neo4j:clear");
        $this->setDescription("Clears the database!");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("Clearing database...");
        $this->cityRepository->deleteEverything();

        return self::SUCCESS;
    }
}