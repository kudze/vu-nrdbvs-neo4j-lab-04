<?php

namespace Kudze\NrdbvsNeo4jDemo\Command;

use Kudze\NrdbvsNeo4jDemo\Repository\PersonRepository;
use Kudze\NrdbvsNeo4jDemo\Service\Neo4JConnector;
use Kudze\NrdbvsNeo4jDemo\Service\TableBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Neo4JListAllPeopleCommand extends Command
{
    private PersonRepository $personRepository;
    private TableBuilder $tableBuilder;

    public function __construct(PersonRepository $personRepository, TableBuilder $tableBuilder)
    {
        $this->personRepository = $personRepository;
        $this->tableBuilder = $tableBuilder;

        parent::__construct();
    }

    public function configure()
    {
        $this->setName("neo4j:list:people");
        $this->setDescription("Lists all people in neo4j database!");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $people = $this->personRepository->listAllPeople();
        $table = $this->tableBuilder->buildTableOfPeople($people);

        $output->writeln("All people in database:");
        $output->writeln($table);

        return self::SUCCESS;
    }
}