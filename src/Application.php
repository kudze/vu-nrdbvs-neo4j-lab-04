<?php

namespace Kudze\NrdbvsNeo4jDemo;

use DI\Container;
use Kudze\NrdbvsNeo4jDemo\Command\HelloWorldCommand;
use Kudze\NrdbvsNeo4jDemo\Command\Neo4JAggregateShortestRoadDistanceBetweenTwoCities;
use Kudze\NrdbvsNeo4jDemo\Command\Neo4JClearCommand;
use Kudze\NrdbvsNeo4jDemo\Command\Neo4JFindAllRoadsBetweenTwoCities;
use Kudze\NrdbvsNeo4jDemo\Command\Neo4JFindShortestRoadBetweenTwoCities;
use Kudze\NrdbvsNeo4jDemo\Command\Neo4JHelloWorldCommand;
use Kudze\NrdbvsNeo4jDemo\Command\Neo4JListAllCitiesCommand;
use Kudze\NrdbvsNeo4jDemo\Command\Neo4JListAllPeopleCommand;
use Kudze\NrdbvsNeo4jDemo\Command\Neo4JSearchCityCitizensCommand;
use Kudze\NrdbvsNeo4jDemo\Command\Neo4JSearchPeopleCommand;
use Kudze\NrdbvsNeo4jDemo\Command\Neo4JSearchPersonCityCommand;
use Kudze\NrdbvsNeo4jDemo\Command\Neo4JSeedCommand;

class Application extends \Symfony\Component\Console\Application
{
    const APP_NAME = "Neo4J Demo";
    const APP_VERSION = "Alpha v0.0.1";

    private Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;

        parent::__construct(self::APP_NAME, self::APP_VERSION);

        $this->registerCommands();
    }

    protected function registerCommands() {
        $this->add($this->container->get(HelloWorldCommand::class));
        $this->add($this->container->get(Neo4JHelloWorldCommand::class));
        $this->add($this->container->get(Neo4JSeedCommand::class));
        $this->add($this->container->get(Neo4JClearCommand::class));
        $this->add($this->container->get(Neo4JListAllPeopleCommand::class));
        $this->add($this->container->get(Neo4JListAllCitiesCommand::class));
        $this->add($this->container->get(Neo4JSearchPeopleCommand::class));
        $this->add($this->container->get(Neo4JSearchPersonCityCommand::class));
        $this->add($this->container->get(Neo4JSearchCityCitizensCommand::class));
        $this->add($this->container->get(Neo4JFindAllRoadsBetweenTwoCities::class));
        $this->add($this->container->get(Neo4JFindShortestRoadBetweenTwoCities::class));
        $this->add($this->container->get(Neo4JAggregateShortestRoadDistanceBetweenTwoCities::class));
    }
}