# VU Neo4J labaratorinis.

# Requirements

PHP 8
Composer

# Startup

1. Rename `.env.example` to `.env`
2. Replace values in .env (common sense)
3. `composer install`
4. `./bin/console` and have fun!